###############
# Constants
###############
DIGITS = '0123456789'
ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
SPECIAL_SYMBOLS = '.$_/<>"'

IN_LABEL_SYMBOLS = DIGITS + ALPHABET + '$_/<>.'
START_OPCODE_SYMBOLS = '.' + '%'
IN_OPERAND_SYMBOLS = DIGITS + ALPHABET + \
    '$_/<>"+-*[]()'  # FPL, operand and label
START_HEADER_FOOTER_SYMBOLS = ':'

################
# Error
################


class Error:
    def __init__(self, error_name, details):
        self.error_name = error_name
        self.details = details

    def as_string(self):
        result = f'{self.error_name}:{self.details}'
        return result


class ErrorIllegalChar(Error):
    def __init__(self, details):
        super().__init__('Illegal Character', details)


class ErrorIllegalStatement(Error):
    def __init__(self, details):
        super().__init__('IllegalStatement', details)


class ErrorUnfinishedStatement(Error):
    def __init__(self, details):
        super().__init__('Unfinished Statement', details)


################
# Tokens
################
TT_HEADER = 'HEADER'      # HEADER/FOOTER code
TT_FOOTER = 'FOOTER'    # HEADER/FOOTER parameter
TT_FPL = 'FPL'        # Footer param line

TT_LABEL = 'LABEL'
TT_OPCODE = 'OPCODE'
TT_CMT = 'CMT'
TT_OPERAND = 'OPERAND'


class Token:
    def __init__(self, type_, string=None):  # val_type = None,value=None
        self.type = type_
        self.string = string
        #self.val_type = val_type
        #self.value = value

    def __repr__(self):
        # if self.value:
        #     return f'{self.type}:{self.string}'
        return f'{self.type}'

#################
# Parser
#################
class Parser:
    def __init__(self, text=None):
        self.scope = ":HEADER"         # :HEADER/ XXX/ :FOOTER
        if text:
            self.set_text(text)

    def set_text(self, text, line_num: int = 0):
        """
        Deal with one statement at most.
        This function will set text and start a new lex session.
        Text should start with the start of a statement.
        """
        self.text = text
        self.pos = -1
        self.current_char = None
        self.after_opcode = False       # True if opcode is passed
        self.after_semicolon = False    # True if the statement is finished

        self.advance()                  # Advance to wait for parse_statement

    def advance(self):
        self.pos += 1
        self.current_char = self.text[self.pos] if self.pos < len(
            self.text) else None

    def skip_chars(self, chars: str = "\t\n\r "):
        while self.current_char and self.current_char in chars:
            self.advance()

    def parse_statement(self, line_num):
        """
        Deal with one statement at most.
        This function will return a dict and a error flag.
        This function will ignore the comments.
        """

        label = ""
        opcode = ""
        operands = []

        if self.current_char in "#;":
            self.after_semicolon = True             # Finish this Line
            return {}, None                         # Whole line of comment
        elif self.scope == ":HEADER" and self.current_char == ":":    # Header
            self.advance()
            opcode = self.make_opcode().string
        elif self.scope == ":FOOTER":
            if self.current_char == ":":
                self.advance()
                opcode = self.make_opcode().string
            # Else go skip, to make operands

        # Not Header nor footer...
        elif self.current_char in ' \t':              # Start with empty.
            self.skip_chars(" \t")
            if self.current_char == ";":
                self.after_semicolon = True
                return {}, None     # Empty
            if self.current_char and self.current_char not in START_OPCODE_SYMBOLS:
                print(self.current_char)
                return {}, ErrorIllegalChar("Opcode should start with '.' or '%'.")
            opcode = self.make_opcode().string
            if opcode == ".scope":
                self.scope = ":SCOPE_JUMPING"
        elif self.current_char and self.current_char in IN_LABEL_SYMBOLS:  # Start with label
            label = self.make_label().string
            self.skip_chars(", \t")
            if self.current_char == ";":
                self.after_semicolon = True
                return {"vvp_line_num": line_num, "label": label, "scope": self.scope}, None
            if self.current_char and self.current_char not in START_OPCODE_SYMBOLS:
                return {}, ErrorIllegalChar("Opcode should start with '.' or '%'.")
            opcode = self.make_opcode().string
            if opcode == ".scope":
                self.scope = label
        elif self.current_char and self.current_char in ":":                # Is footer
            self.scope = ":FOOTER"
            self.advance()                                                  # Skip ":"
            opcode = self.make_opcode().string
        else:
            return {}, ErrorIllegalChar("Unknown start of statement.")

        # Make operands
        self.skip_chars(", \t")
        while self.current_char and self.current_char in IN_OPERAND_SYMBOLS:
            _operand = self.make_operand().string
            operands.append(_operand)
            if self.scope == ":SCOPE_JUMPING":                  # Temporary scope
                self.scope = _operand
            self.skip_chars(", \t")

        # Check if scope is determined.
        if self.scope == ":SCOPE_JUMPING":
            return {}, ErrorIllegalStatement(
                "The short form of .scope should have a defined scope label as first operand."
            )

        if self.current_char == ";":
            self.after_semicolon = True
        return {"vvp_line_num": line_num, "label": label, "opcode": opcode, "scope": self.scope, "operands": operands}, None
        # Output the line number to the tokens

    def make_label(self):
        label_str = ''
        while self.current_char and self.current_char in IN_LABEL_SYMBOLS:
            label_str += self.current_char
            self.advance()
        return Token(TT_LABEL, string=label_str)

    def make_opcode(self):
        opcode_str = ''
        while self.current_char and self.current_char not in ' ;,':
            opcode_str += self.current_char
            self.advance()
        self.after_opcode = True
        return Token(TT_OPCODE, string=opcode_str)

    def make_cmt(self):
        """
        OBSOLETE
        """
        cmt_str = ''
        while self.current_char:
            cmt_str += self.current_char
            self.advance()
            # comment does not include new line.
            cmt_str = cmt_str.strip('\n')
        return Token(TT_CMT, string=cmt_str)

    def make_operand(self):
        operand_str = ''
        while self.current_char and self.current_char not in ',' + ';':

            if self.current_char == "(":
                operand_str += self.get_barc_str()
            elif self.current_char == "\"":
                operand_str += self.get_quote_str()
            else:
                operand_str += self.current_char
                self.advance()
        return Token(TT_OPERAND, string=operand_str.rstrip())

    def get_quote_str(self):
        start_char = self.current_char
        assert start_char in "\"", "Start with non-quote mark"
        res_str = ""

        res_str += self.current_char    # A; A and B are to keep the quote marks
        self.advance()
        while self.current_char != start_char:
            res_str += self.current_char
            self.advance()
        res_str += self.current_char    # B; A and B are to keep the quote marks
        self.advance()
        return res_str

    def get_barc_str(self):
        start_char = self.current_char
        assert start_char == "("
        barc_count = 1
        res_str = ""

        res_str += self.current_char
        self.advance()
        while barc_count > 0:
            if self.current_char == '"':
                res_str += self.get_quote_str()
            elif self.current_char == "(":
                barc_count += 1
                res_str += self.current_char
                self.advance()
            elif self.current_char == ")":
                barc_count -= 1
                res_str += self.current_char
                self.advance()
            else:
                res_str += self.current_char
                self.advance()
        return res_str

    def run(self, text, line_num):
        """
        Run a single statement.
        """
        self.set_text(text)
        tokens, error = self.parse_statement(line_num)
        # Mark line finished if error occured.
        return tokens, error, self.after_semicolon if not error else True
