from json_tools import JsonExportable


class Signal(JsonExportable):
    _exports = ["type", "scope", "msb", "lsb", "arr_last","arr_first","name", "val_labels",]

    # Constants
    T_PARAM = "T_PARAM"
    T_VAR_ARR = "T_VAR_ARR"
    T_NET_ARR = "T_NET_ARR"
    T_VAR = "T_VAR"
    T_NET = "T_NET"
    T_NET_ARR_CELL = "T_NET_ARR_CELL"   # net is a array-cell

    def __init__(
        self,
        type_: str,
        scope: str,
        msb: int = 0,
        lsb: int = 0,
        arr_last: int = 0,    # not export
        arr_first: int = 0,      # not export
        name: str = "",
        # Where the value lays. For net, it is the driver, for var, it is the var itself.
        val_labels: list = [],
    ):
        self.type = type_
        self.scope = scope
        self.msb = int(msb)
        self.lsb = int(lsb)
        self.arr_last = arr_last
        self.arr_first = arr_first
        self.name = name
        self.val_labels = val_labels
    
    def set_bit_range(self,msb,lsb):
        self.msb = int(msb)
        self.lsb = int(lsb)

