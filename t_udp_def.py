from json_tools import JsonExportable

class UdpDef(JsonExportable):
    _exports = ["name","input_amount","table_rows","init_val"]
    def __init__(
        self,
        name :str,
        input_amount: int,
        table_rows: list,
        init_val: str="",      # Not empty means is sequential
    ):
        assert type(name) == str
        # assert type(input_amount) == int
        assert type(table_rows) == list
        assert type(init_val) == str

        self.name = name
        self.input_amount = int(input_amount)
        self.table_rows = table_rows
        self.init_val = init_val

