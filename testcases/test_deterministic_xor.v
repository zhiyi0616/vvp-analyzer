module deterministic_xor();
    reg a;
    wire c;
    xor (c,a,a);
    initial begin
        $dumpfile("deterministic_xor.vcd");
        $dumpvars(0,deterministic_xor);
        #10 a = 1'b1;
        #10 a = 1'b0;
        #10 a = 1'bx;   // Should be 0
        #10 a = 1'bz;
        #10 a = 1'b1;
    end
endmodule