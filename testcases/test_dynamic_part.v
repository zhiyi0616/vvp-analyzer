module test_dynamiv_part();
    wire [4:0]a;
    wire [1:0]b;
    wire c;
    assign c = a[b];
endmodule


/**

o0000000000f86fd8 .functor BUFZ 5, C4<zzzzz>; HiZ drive
v0000000000f855c0_0 .net "a", 4 0, o0000000000f86fd8;  0 drivers
o0000000000f87008 .functor BUFZ 2, C4<zz>; HiZ drive
v0000000000f85660_0 .net "b", 1 0, o0000000000f87008;  0 drivers
v0000000000f85700_0 .net "c", 0 0, L_0000000000f857a0;  1 drivers
L_0000000000f857a0 .part/v o0000000000f86fd8, o0000000000f87008, 1;

*/