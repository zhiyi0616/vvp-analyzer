module test_hamming16_tb();
    reg [15:0] source;
    reg [4:0] chan_flip_bit;
    wire [20:0] encoded;
    wire [20:0] received;
    wire [15:0] decoded;
    wire [4:0] check_tb;

    encode_hamming16 dut1(source,encoded);
    channel_change1_16 dut3(encoded,chan_flip_bit,received);
    decode_hamming16 dut2(received,decoded,check_tb);
    
    initial begin 
        $dumpfile("hamming.vcd");
        $dumpvars(0,test_hamming16_tb);
        chan_flip_bit = 0;
        #10 source = 16'h00;
        #10 source = 16'h01;
        #10 source = 16'h02;
        #10 chan_flip_bit = 2;
        #10 source = 16'h21;
        #10 source = 16'hf3;
        #10 source = 16'h47;
        #10 $finish;
    end
endmodule

module channel_change1_16(in,flip_bit_id,out);
    input wire [21:1]in;
    input wire [4:0] flip_bit_id;
    output wire [21:1]out;

    assign out[01]= flip_bit_id==5'd01?~in[01]:in[01];
    assign out[02]= flip_bit_id==5'd02?~in[02]:in[02];
    assign out[03]= flip_bit_id==5'd03?~in[03]:in[03];
    assign out[04]= flip_bit_id==5'd04?~in[04]:in[04];
    assign out[05]= flip_bit_id==5'd05?~in[05]:in[05];
    assign out[06]= flip_bit_id==5'd06?~in[06]:in[06];
    assign out[07]= flip_bit_id==5'd07?~in[07]:in[07];
    assign out[08]= flip_bit_id==5'd08?~in[08]:in[08];
    assign out[09]= flip_bit_id==5'd09?~in[09]:in[09];
    assign out[10]= flip_bit_id==5'd10?~in[10]:in[10];
    assign out[11]= flip_bit_id==5'd11?~in[11]:in[11];
    assign out[12]= flip_bit_id==5'd12?~in[12]:in[12];
    assign out[13]= flip_bit_id==5'd13?~in[13]:in[13];
    assign out[14]= flip_bit_id==5'd14?~in[14]:in[14];
    assign out[15]= flip_bit_id==5'd15?~in[15]:in[15];
    assign out[16]= flip_bit_id==5'd16?~in[16]:in[16];
    assign out[17]= flip_bit_id==5'd17?~in[17]:in[17];
    assign out[18]= flip_bit_id==5'd18?~in[18]:in[18];
    assign out[19]= flip_bit_id==5'd19?~in[19]:in[19];
    assign out[20]= flip_bit_id==5'd20?~in[20]:in[20];
    assign out[21]= flip_bit_id==5'd21?~in[21]:in[21];

endmodule

module encode_hamming16(
    in,     // Source info bits
    out     // Encoded bits
);
    input wire [16:1] in;
    output wire [21:1] out;
    assign out[01]=in[1]^ in[2]^ in[4]^ in[5]^ in[7]^in[9]^ in[11]^in[12]^in[14]^in[16];
    assign out[02]=in[1]^ in[3]^ in[4]^ in[6]^ in[7]^in[10]^in[11]^in[13]^in[14];
    assign out[04]=in[2]^ in[3]^ in[4]^ in[8]^ in[9]^in[10]^in[11]^in[15]^in[16];
    assign out[08]=in[5]^ in[6]^ in[7]^ in[8]^ in[9]^in[10]^in[11];
    assign out[16]=in[12]^in[13]^in[14]^in[15]^in[16];

    assign out[03]=in[01];
    assign out[05]=in[02];
    assign out[06]=in[03];
    assign out[07]=in[04];
    assign out[09]=in[05];
    assign out[10]=in[06];
    assign out[11]=in[07];
    assign out[12]=in[08];
    assign out[13]=in[09];
    assign out[14]=in[10];
    assign out[15]=in[11];
    assign out[17]=in[12];
    assign out[18]=in[13];
    assign out[19]=in[14];
    assign out[20]=in[15];
    assign out[21]=in[16];
endmodule



module decode_hamming16(
    in,     // Encoded
    out,    // Decoded
    check
);
    input wire [21:1] in;
    output wire[16:1] out;
    output wire [5:1]check;
    wire [21:1]corrected;

    assign check[1] = in[1]^in[3]^in[5]^in[7]^in[9]^in[11]^in[13]^in[15]^in[17]^in[19]^in[21];
    assign check[2] = in[2]^in[3]^in[6]^in[7]^in[10]^in[11]^in[14]^in[15]^in[18]^in[19];
    assign check[3] = in[04]^in[05]^in[06]^in[07]^in[12]^in[13]^in[14]^in[15]^in[20]^in[21];
    assign check[4] = in[08]^in[09]^in[10]^in[11]^in[12]^in[13]^in[14]^in[15];
    assign check[5] = in[16]^in[17]^in[18]^in[19]^in[20]^in[21];
    // use check to correct in
    // Here we would like to use always FIXME

    // correct bits
    assign corrected[01] = check==5'd01?~in[01]:in[01];
    assign corrected[02] = check==5'd02?~in[02]:in[02];
    assign corrected[03] = check==5'd03?~in[03]:in[03];
    assign corrected[04] = check==5'd04?~in[04]:in[04];
    assign corrected[05] = check==5'd05?~in[05]:in[05];
    assign corrected[06] = check==5'd06?~in[06]:in[06];
    assign corrected[07] = check==5'd07?~in[07]:in[07];
    assign corrected[08] = check==5'd08?~in[08]:in[08];
    assign corrected[09] = check==5'd09?~in[09]:in[09];
    assign corrected[10] = check==5'd10?~in[10]:in[10];
    assign corrected[11] = check==5'd11?~in[11]:in[11];
    assign corrected[12] = check==5'd12?~in[12]:in[12];
    assign corrected[13] = check==5'd13?~in[13]:in[13];
    assign corrected[14] = check==5'd14?~in[14]:in[14];
    assign corrected[15] = check==5'd15?~in[15]:in[15];
    assign corrected[16] = check==5'd16?~in[16]:in[16];
    assign corrected[17] = check==5'd17?~in[17]:in[17];
    assign corrected[18] = check==5'd18?~in[18]:in[18];
    assign corrected[19] = check==5'd19?~in[19]:in[19];
    assign corrected[20] = check==5'd20?~in[20]:in[20];
    assign corrected[21] = check==5'd21?~in[21]:in[21];

    // re-arrange bits
    assign out[01]=corrected[03];
    assign out[02]=corrected[05];
    assign out[03]=corrected[06];
    assign out[04]=corrected[07];
    assign out[05]=corrected[09];
    assign out[06]=corrected[10];
    assign out[07]=corrected[11];
    assign out[08]=corrected[12];
    assign out[09]=corrected[13];
    assign out[10]=corrected[14];
    assign out[11]=corrected[15];
    assign out[12]=corrected[17];
    assign out[13]=corrected[18];
    assign out[14]=corrected[19];
    assign out[15]=corrected[20];
    assign out[16]=corrected[21];
endmodule





