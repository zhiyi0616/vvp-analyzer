module decode_hamming16(
    in,     // Encoded
    out,    // Decoded
    check
);
    input wire [6:1] in;
    output wire[3:1] out;
    output wire [3:1]check;
    wire [6:1]corrected;

    assign check[1] = in[1]^in[3]^in[5];
    assign check[2] = in[2]^in[3]^in[6];
    assign check[3] = in[04]^in[05]^in[06];

    // use check to correct in
    // Here we would like to use always FIXME

    // correct bits
    assign corrected[01] = check==5'd01?~in[01]:in[01];
    assign corrected[02] = check==5'd02?~in[02]:in[02];
    assign corrected[03] = check==5'd03?~in[03]:in[03];
    assign corrected[04] = check==5'd04?~in[04]:in[04];
    assign corrected[05] = check==5'd05?~in[05]:in[05];
    assign corrected[06] = check==5'd06?~in[06]:in[06];

    // re-arrange bits
    assign out[01]=corrected[03];
    assign out[02]=corrected[05];
    assign out[03]=corrected[06];

endmodule