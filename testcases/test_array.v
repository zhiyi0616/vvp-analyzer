`timescale 1us/1ns      // Should outside a module
module test_array();
    wire [7:0] a,b,x_,y;
    wire [7:0] c=8'b10101010;
    wire [6:-1] arr_net[2:1];   // Can use negtive numbers. Range will be stored in corresponding nets
    reg [7:0] arr_var[2:1];     // Range will be stored in .array

    assign arr_net[1] = c;
    assign a = arr_net[1];      // in vvp, refer 0
    assign b = arr_var[1];      // in vvp, refer 0
    assign x_ = arr_net[b];

endmodule

