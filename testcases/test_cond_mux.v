module test_cond_mux();
    wire [1:0] a;
    wire [3:0] b;
    wire [4:0] c;
    wire [6:0] d;
    assign b = 3;
    assign c = a+b;
    assign d = a+b;
    assign a = b?c:d;
    // condition 位宽等于 cd 中最宽
    // a 只取 condition 输出的前两位（assign 等号两边不相等的情况）
    // 条件 b 会和 buft 常量输出比较作为条件
endmodule
// 注意 buft 号称位宽是 1 但是不一定。