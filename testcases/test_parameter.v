// S_0000000000f45430 .scope module, "test_param" "test_param" 2 1;
module test_param();
    // P_0000000000f465e0 .param/l "pr" 0 2 2, +C4<00000000000000000000000000000101>;
    parameter pr=5;
    parameter pr2=3'b101;
    parameter pr3=-1;
    wire [1:0] a;
    // v0000000000f455c0 .array "arr", 5 0, 1 0;
    reg [1:0] arr[0:pr];
    
    // v0000000000f455c0_1 .array/port v0000000000f455c0, 1;
    // L_0000000000f45930/d .functor BUFZ 2, v0000000000f455c0_1, C4<00>, C4<00>, C4<00>;
    // L_0000000000f45930 .delay 2 (5,5,5) L_0000000000f45930/d;
    // v0000000000096ac0_0 .net "a", 1 0, L_0000000000f45930;  1 drivers
    assign #pr a = arr[1];
endmodule
