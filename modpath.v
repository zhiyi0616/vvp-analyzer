module modpath(o,a,b,c);
input a,b,c;
output o;
wire o_nor;

specify
    (a=>o) = 3;
    // (b=>o) = 2;
    // (c=>o) = 3;
    specparam PATHPULSE$a$o = 2;
endspecify

nor n1(o_nor,a,b);
or o1(o,o_nor,c);
endmodule
