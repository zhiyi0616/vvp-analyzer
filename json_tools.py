
class JsonExportable:
    _exports = {}
    def to_dict(self):
        res_dict = {}
        if isinstance(self._exports,dict):
            for attr_key, target_key in self._exports.items():
                res_dict[target_key] = getattr(self, attr_key)
        elif isinstance(self._exports,list):
            for dict_key in self._exports:
                res_dict[dict_key] = getattr(self, dict_key)
        else:
            raise Exception("Class variable _exports is not dict nor list.")
        return res_dict

class Placeholder(JsonExportable):
    def to_dict(self):
        raise Exception("Placeholder is NOT exportable...")

def to_dict(obj):
    
    if isinstance(obj, JsonExportable):
        # print("RRRAAA, to_dict is called...")
        return obj.to_dict()
    else:
        # print("AAA, to_dict is called...")
        return obj.__dict__
