# Nodes

| Type Name         | VVP Opcpde      | Supported |
| ----------------- | --------------- | --------- |
| N_VAR             | .var            | Y         |
| N_VAR_COBJ        | .var/cobj       | N         |
| N_VAR_DARRAY      | .var/darray     | N         |
| N_VAR_QUEUE       | .var/queue      | N         |
| N_VAR_REAL        | .var/real       | N         |
| N_VAR_S           | .var/s          | N         |
| N_VAR_STR         | .var/str        | N         |
| N_VAR_I           | .var/i          | N         |
| N_VAR_2S          | .var/2s         | N         |
| N_VAR_2U          | .var/2u         | N         |
| N_ABS             | .abs            | *         |
| N_ARITH_DIV       | .arith/div      | *         |
| N_ARITH_DIV_R     | .arith/div.r    | N         |
| N_ARITH_DIV_S     | .arith/div.s    | N         |
| N_ARITH_MOD       | .arith/mod      | *         |
| N_ARITH_MOD_R     | .arith/mod.r    | N         |
| N_ARITH_MOD_S     | .arith/mod.s    | N         |
| N_ARITH_MULT      | .arith/mult     | *         |
| N_ARITH_MULT_R    | .arith/mult.r   | N         |
| N_ARITH_POW       | .arith/pow      | N         |
| N_ARITH_POW_R     | .arith/pow.r    | N         |
| N_ARITH_POW_S     | .arith/pow.s    | N         |
| N_ARITH_SUB       | .arith/sub      | Y         |
| N_ARITH_SUB_R     | .arith/sub.r    | N         |
| N_ARITH_SUM       | .arith/sum      | Y         |
| N_ARITH_SUM_R     | .arith/sum.r    | N         |
| N_ARRAY_PORT      | .array/port     | *         |
| N_FUNC_AND        | .functor AND    | Y         |
| N_FUNC_OR         | .functor OR     | Y         |
| N_FUNC_NAND       | .functor NAND   | Y         |
| N_FUNC_NOR        | .functor NOR    | Y         |
| N_FUNC_XNOR       | .functor XNOR   | Y         |
| N_FUNC_XOR        | .functor XOR    | Y         |
| N_FUNC_NOT        | .functor NOT    | Y         |
| N_FUNC_NOTIF0     | .functor NOTIF0 | Y *       |
| N_FUNC_NOTIF1     | .functor NOTIF1 | Y *       |
| N_FUNC_BUF        | .functor BUF    | Y *       |
| N_FUNC_BUFIF0     | .functor BUFIF0 | Y *       |
| N_FUNC_BUFIF1     | .functor BUFIF1 | Y *       |
| N_FUNC_BUFT       | .functor BUFT   | Y *       |
| N_FUNC_BUFZ       | .functor BUFZ   | Y *       |
| N_FUNC_MUXR       | .functor MUXR   | Y *       |
| N_FUNC_MUXZ       | .functor MUXZ   | Y *       |
| N_FUNC_CMOS       | .functor CMOS   | Y *       |
| N_FUNC_NMOS       | .functor NMOS   | Y *       |
| N_FUNC_PMOS       | .functor PMOS   | Y *       |
| N_FUNC_RCMOS      | .functor RCMOS  | Y *       |
| N_FUNC_RNMOS      | .functor RNMOS  | Y *       |
| N_FUNC_RPMOS      | .functor RPMOS  | Y *       |
| N_CMP_EEQ         | .cmp/eeq        | Y         |
| N_CMP_EQX         | .cmp/eqx        | *         |
| N_CMP_EQZ         | .cmp/eqz        | *         |
| N_CMP_EQ          | .cmp/eq         | Y         |
| N_CMP_EQ_R        | .cmp/eq.r       | N         |
| N_CMP_NEE         | .cmp/nee        | Y         |
| N_CMP_NE          | .cmp/ne         | Y         |
| N_CMP_NE_R        | .cmp/ne.r       | N         |
| N_CMP_GE          | .cmp/ge         | *         |
| N_CMP_GE_R        | .cmp/ge.r       | N         |
| N_CMP_GE_S        | .cmp/ge.s       | N         |
| N_CMP_GT          | .cmp/gt         | *         |
| N_CMP_GT_R        | .cmp/gt.r       | N         |
| N_CMP_GT_S        | .cmp/gt.s       | N         |
| N_CMP_WEQ         | .cmp/weq        | *         |
| N_CMP_WNE         | .cmp/wne        | *         |
| N_CONCAT          | .concat         | *         |
| N_CONCAT8         | .concat8        | Y         |
| N_DELAY           | .delay          | *         |
| N_DFF_N           | .dff/n          | N         |
| N_DFF_N_ACLR      | .dff/n/aclr     | N         |
| N_DFF_N_ASET      | .dff/n/aset     | N         |
| N_DFF_P           | .dff/p          | N         |
| N_DFF_P_ACLR      | .dff/p/aclr     | N         |
| N_DFF_P_ASET      | .dff/p/aset     | N         |
| N_LATCH           | .latch          | N         |
| N_EVENT_ANYEDGE   | .event edge     | *         |
| N_EVENT_POSEDGE   | .event posedge  | *         |
| N_EVENT_NEGEDGE   | .event negedge  | *         |
| N_EVENT_OR        | .event/or       | *         |
| N_EXTEND_S        | .extend/s       | N         |
| N_TCAST_TO_2      | .cast/2         | N         |
| N_TCAST_TO_INT    | .cast/int       | N         |
| N_TCAST_TO_REAL   | .cast/real      | N         |
| N_TCAST_TO_REAL_S | .cast/real.s    | N         |
| N_MODPATH         | .modpath        | N         |
| N_VPART_SEL       | .part           | *         |
| N_VPART_DRIVER    | .part/pv        | *         |
| N_VPART_SEL_V     | .part/v         | *         |
| N_VPART_SEL_V_S   | .part/v.s       | *         |
| N_ISLAND_PORT     | .port           | N         |
| N_ISLAND_IMPORT   | .import         | N         |
| N_ISLAND_EXPORT   | .export         | N         |
| N_REDUCE_AND      | .reduce/and     | *         |
| N_REDUCE_OR       | .reduce/or      | *         |
| N_REDUCE_XOR      | .reduce/xor     | *         |
| N_REDUCE_NAND     | .reduce/nand    | *         |
| N_REDUCE_NOR      | .reduce/nor     | *         |
| N_REDUCE_XNOR     | .reduce/xnor    | *         |
| N_REPEAT          | .repeat         | *         |
| N_STRENGTH_RESOLV | .resolv         | *         |
| N_SHIFT_L         | .shift/l        | *         |
| N_SHIFT_R         | .shift/r        | *         |
| N_SHIFT_RS        | .shift/rs       | *         |
| N_SUBSTITUTE      | .substitute     | N         |
| N_UFUNC_REAL      | .ufunc/real     | N         |
| N_UFUNC_VEC4      | .ufunc/vec4     | N         |
| N_UFUNC_E         | .ufunc/e        | N         |
| N_UDP             | .udp            | *         |
| N_SFUNC           | .sfunc          | N         |
| N_SFUNC_E         | .sfunc/e        | N         |

# Non-nodes
| VVP Opcpde  | Type Name      | Supported |
| ----------- | -------------- | --------- |
| .array      |                |           |
| .array/2s   |                |           |
| .array/2u   |                |           |
| .array/i    |                |           |
| .array/obj  |                |           |
| .array/real |                |           |
| .array/s    |                |           |
| .array/str  |                |           |
| .class      |                |           |
| .enum2      |                |           |
| .enum2/s    |                |           |
| .enum4      |                |           |
| .enum4/s    |                |           |
| .island     |                |           |
| .net        | NET or ARR_NET |           |
| .net/2s     |                |           |
| .net/2u     |                |           |
| .net8       |                |           |
| .net8/2s    |                |           |
| .net8/2u    |                |           |
| .net8/s     |                |           |
| .net/real   |                |           |
| .net/s      |                |           |
| .param/l    |                |           |
| .param/str  |                |           |
| .param/real |                |           |
| .port_info  | SCOPE_PORT     | *         |
| .rtran      |                |           |
| .rtranif0   |                |           |
| .rtranif1   |                |           |
| .scope      |                |           |
| .thread     |                |           |
| .timescale  |                |           |
| .tran       |                |           |
| .tranif0    |                |           |
| .tranif1    |                |           |
| .tranvp     |                |           |
| .udp/c      |                |           |
| .udp/s      |                |           |
