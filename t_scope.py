from json_tools import JsonExportable, Placeholder

class Scope(JsonExportable):
    _exports = [
        "type", "inst_name","def_name","parent","children","ancestor_chain", "signals",
        "ports","time_unit","time_precision"
    ]
    def __init__(
        self,
        type_:str,
        inst_name:str,
        def_name:str,
        parent:str,
        ports:dict = {},

    ):
        self.children = {}
        self.ancestor_chain = []
        self.signals = {}
        self.time_unit = Placeholder()
        self.time_precision = Placeholder()

        self.type = type_
        self.inst_name = inst_name
        self.def_name = def_name
        self.parent = parent
        self.ports = ports

    def set_timescale(self,unit, precision):
        self.time_unit = unit
        self.time_precision = precision

    def add_signal(self, name, label):
        self.signals[name] = label
    
    def add_child(self, inst_name, label):
        self.children[inst_name] = label

class ScopePort(JsonExportable):
    _exports = ["type", "inputs", "scope", "scope", "params"]
    def __init__(
        self,

    ):
        pass


