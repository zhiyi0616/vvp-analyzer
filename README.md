# vvp Analyzer

# Node 类型
详见 [cell_types.md](cell_types.md)

# 接口文档
详见 [接口文档.md](接口文档.md)

# 已知的 VVP 节点输出宽度问题
- functor BUFZ 宽度为1，但是输出宽度不一定为1，应该以第一输入的宽度为准
- cmp 类型的宽度为输入宽度。其输出宽度一定是1
- 乘法结果宽度可能会大于原先宽度
- concat 宽度为输入宽度列表
