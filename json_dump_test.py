import json


class JsonExportable:
    _export = ["tt"]
    # def __init__(self, export_keys: list):
    #     self.__export_keys = export_keys

    def to_dict(self):
        res_dict = {}
        # print("bbb",Node.export_keys)
        for dict_key in self._export_keys:
            res_dict[dict_key] = getattr(self, dict_key)
        return res_dict



def to_dict(obj):
    
    if isinstance(obj, JsonExportable):
        # print("RRRAAA, to_dict is called...")
        return obj.to_dict()
    else:
        # print("AAA, to_dict is called...")
        return obj.__dict__


class Node(JsonExportable):
    _export = ["type", "inputs","scope", "params"]
    def __init__(
        self,
        type_: str,
        inputs: list, # list of NodeInput
        scope: str,
        params: dict = {},
    ):
        print("aaa",Node._export_keys)
        # super().__init__()
        self.type = type_
        self.inputs = inputs
        self.scope = scope
        self.params = params

a = {
    "ddf":Node(
        type_ = "qqq",
        inputs = ["dd","ddt"],
        scope = "--",
    )
}

f = open("dfdf.json","+w")
json.dump(a,f,default= to_dict)
