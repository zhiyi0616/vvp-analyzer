
from type_name_table import type_name_table
from json_tools import JsonExportable
from t_node import Node, NodeInput
from t_signal_param import Signal
from t_scope import Scope
from t_udp_def import UdpDef
from t_proc_thread import VThread


def parse_concat_widths(str_):
    str_ = str_.strip()
    assert str_[0] == '[' and str_[-1] == ']'
    str_ = str_[1:-1]
    res = str_.split()
    res = [int(x) for x in res]
    return res


def quote_remove(name_str: str):
    if len(name_str) >= 2:
        if name_str[0] == "\""and name_str[-1] == "\"":
            return name_str[1:-1]
        else:
            return "*"
    elif name_str == "":
        return name_str
    else:
        return "*"

def get_brac_str(str):
    brac_str = ""
    for char in str:
        brac_str += char
        if char == "(":
            brac_str = "("
        elif char == ")":
            break
    return brac_str


def make_inputs(input_list: list):
    return [
        NodeInput(input_str) for input_str in input_list
    ]


def fill_buckets(
    verbose_flag,
    statement_list,
    # label_index_dict
):
    node_dict = {}          # All netlist nodes
    signal_dict = {}        # All signals
    thread_dict = {}        # All threads
    scope_dict = {}         # All scopes
    udp_def_dict = {}       # UDP definitions

    code_segment = []       # All procedure codes(v-instructions)

    # Temp
    label_vinst_index = {}  # "label": vinst_index
    # Events may refer to nets, so this is used to store event-net index
    event_net_index = {}
    out_width_unset_list = []

    # parameter_dict = {}     # All parameters
    # scope_port_list = []    # To help attach ports into scopes

    # This will not change the original list.
    current_vinst_index = 0
    for statement in statement_list:
        label = statement.get("label")
        opcode = statement.get("opcode")
        scope = statement.get("scope")

        # Add index to the procedural codes(v instructions)
        if label:
            label_vinst_index[label] = current_vinst_index

        # Every statement should belong to a scope
        if not scope:
            print("Error: Statement without scope", statement)

        # [Node] functor
        if opcode and opcode == ".functor":
            split_blank = statement["operands"][0].split(' ')
            if len(split_blank)>2:
                print("Unknown type of functor:",statement)
            functor = split_blank[0]
            width = split_blank[1]
            if functor == "BUFT":
                inputs=make_inputs([statement["operands"][1]])
                width = len(inputs[0].val_str) if inputs[0].is_const else width # FIXME id buft have non-const val
            else:
                inputs=make_inputs(statement["operands"][1:])
                
            node_dict[label] = Node(
                type_=type_name_table[".functor " + functor],  # look-up
                inputs=inputs,
                width=width,
                scope=scope,
            )

        # [Node] Strength-aware functor statements
        elif opcode and opcode == ".resolv":
            """
            <label> .resolv XXX,  <symbols_list>;
                tri
                tri0
                tri1
                triand
                trior
            The output from the resolver is vvp_vector8_t value
            """
            resol_func = statement["operands"][0]
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                inputs=make_inputs(statement["operands"][1:]),
                scope=scope,
                width=-1,                            # FIXME: There should be a width
                params={"resol_func": resol_func}   # FIXME: separate opcode.
            )

        # [Node] Part select  of vec
        elif opcode and opcode == ".part":
            """
            <label> .part <symbol>, <base>, <wid>;
            <label> .part/v <symbol>, <symbol>, <wid>;
            <label> .part/v.s <symbol>, <symbol>, <wid>;
            The .part/v variation takes a vector (or long) input on port-1 as the
            base of the part select.
            """
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                # Inputs{0:input, 1:base}
                inputs=make_inputs([statement["operands"][0]]),
                scope=scope,
                width=statement["operands"][2],
                # FIXME: should be Port 1
                params={
                    "base": statement["operands"][1]
                    }
            )
        # [NODE] Partial driver, REVERSE too part select
        elif opcode and opcode == ".part/pv":
            """
            <label> .part/pv <symbol>, <base>, <wid>, <vector_wid>;
            The .part/pv variation is the inverse of the .part version
            """
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                # Inputs{0:input, 1:base}
                inputs=make_inputs([statement["operands"][0]]),
                scope=scope,
                width=statement["operands"][3], # Target input width 
                params={
                    "target_port_offset": statement["operands"][1], # From right(LSB)
                    "drive_width":statement["operands"][2],
                }
            )
        elif opcode and opcode.split("/")[0] == ".part":
            print("Unsupported .part statement:", opcode)

        # [Node] Delay FIXME: there is another type of delay.
        elif opcode and opcode == ".delay":
            if len(statement["operands"]) == 1:
                split_blank = statement["operands"][0].split(" ")
                node_dict[label] = Node(
                    type_=type_name_table[opcode],  # look-up
                    # Inputs{0:input， 1:raise, 2:fall, 3:decay} FIXME
                    inputs=make_inputs(split_blank[2:]),
                    scope=scope,
                    width=split_blank[0],
                    # FIXME: Separate...
                    params={"rise_fall_decay": split_blank[1]}
                )
            else:
                raise Exception(
                    "Unsupported type of delay, maybe dynamic delay.")

        # [Node] Output the concatenation of all the inputs.
        elif opcode and opcode in [".concat", ".concat8"]:
            input_widths = parse_concat_widths(statement["operands"][0])
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                inputs=make_inputs(statement["operands"][1:]),
                scope=scope,
                width=sum(input_widths),
                params={}
            )

        # [Node] In-array selection FIXME: undone
        elif opcode and opcode == ".array/port":
            """
            Newly added opcode. Not defined in readme.
            <label> .array/port <arr>, <index>;
            """
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                inputs=make_inputs([statement["operands"][1]]),
                scope=scope,
                width=-1,  # FIXME: statement["operands"][0]
                params={"array_label": statement["operands"][0]}
            )
            out_width_unset_list.append(label)

        # [Node] Shift opcodes
        elif opcode and opcode in [".shift/l", ".shift/r", ".shift/rs"]:
            """
            <label> .shift/l <wid>, <data symbol>, <shift symbol>;
            <label> .shift/r <wid>, <data symbol>, <shift symbol>;

            The shifter has a width that defines the vector width of the output, a
            <data symbol> that is the input data to be shifted and a <shift-symbol>
            that is the amount to shift.
            """
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                # port0: data-symbol; port-1: shift-symbol
                inputs=make_inputs([statement["operands"][1],statement["operands"][2]]),
                scope=scope,
                width=statement["operands"][0],
            )

        # [Node] Arithmetic opcodes
        elif opcode and opcode in [
            ".abs",
            ".arith/sum", ".arith/sum.r",
            ".arith/sub", ".arith/sub.r",
            ".arith/mult", ".arith/mult.r",
            ".arith/div", ".arith/div.r", ".arith/div.s",
            ".arith/pow", ".arith/pow.r", ".arith/pow.s",
            ".arith/mod", ".arith/mod.r", ".arith/mod.s",
        ]:
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                inputs=make_inputs(statement["operands"][1:]),
                scope=scope,
                width=statement["operands"][0],  # output
                params={}
            )

        # [Node] Structural compare statements
        elif opcode and opcode in [".cmp/eeq", ".cmp/eq", ".cmp/ne", ".cmp/gt", ".cmp/ge", ".cmp/ge.s", ".cmp/gt.s"]:
            """
            gt means greater than;
            ge means greater than or equal;
            the ".s" means signed comparisons.
            """
            node_dict[label] = Node(
                type_=type_name_table[opcode],      # look-up
                inputs=make_inputs(statement["operands"][1:]),
                scope=scope,
                # width=statement["operands"][0],     # output
                width=1,            # FIXME: This is a hotfix. the out width should be 1
                params={}
            )
        elif opcode and opcode.split("/")[0] == ".cmp":
            print("Unsupported .cmp statement:", opcode)

        # [Node] Reduction logic
        elif opcode and opcode in [".reduce/and", ".reduce/or", ".reduce/xor", ".reduce/xnor", ".reduce/nor", ".reduce/nand"]:
            """
            The reduction logic statements take in a single vector, and propagate
            a single bit
            <label> .reduce/xxx  <symbol> ;
            """
            node_dict[label] = Node(
                type_=type_name_table[opcode],          # look-up
                # Actually only one.
                inputs=make_inputs(statement["operands"][0:]),
                scope=scope,
                width=1,  # output
                params={}
            )
        elif opcode and opcode.split("/")[0] == ".reduce":
            print("Unsupported .reduce statement:", opcode)

        # [Node] Signed expansion logic
        elif opcode and opcode in [".expand/s", ".extend/s"]:
            """
            .expand/s is defined in the README.txt, however we can only find 
            .extend/s in the source code.

            Sign extension nodes are the opposite of reduction logic, in that they
            take a narrow vector, or single bit, and pad it out to a wider
            vector.

            <label> .ex??nd/s <wid>, <symbol> ;
            """
            node_dict[label] = Node(
                type_=type_name_table[".extend/s"],             # look-up
                # Actually only one.
                inputs=make_inputs(statement["operands"][1:]),
                scope=scope,
                width=statement["operands"][0],                 # Output
                params={}
            )

        # [Node] DFF statements
        elif opcode and opcode in [".dff/p", ".dff/n", ".dff/p/aclr", ".dff/n/aclr", ".dff/p/aset", ".dff/n/aset"]:
            """
            We did not find any .dff during testing.
            """
            print("DFF statements hasn't been supported yet:", statement)

        # [Node] Latch statements
        elif opcode and opcode == ".latch":
            """
            We did not find any .latch during testing.

            <en> is a single bit vector (or scalar) on port-1. Port-0 is any
            type of datum
            """
            split_blank = statement["operands"][0].split(" ")
            node_dict[label] = Node(
                type_=type_name_table[opcode],             # look-up
                # d_input and en
                inputs=make_inputs([split_blank[1], statement["operands"][1]]),
                scope=scope,
                width=split_blank[0],                 # Output
                params={}
            )

        # [Node] Type casting statements to int/vec
        elif opcode and opcode in [".cast/int", ".cast/2"]:
            """
            Type convertion
                <label> .cast/int <width>, <symbol>;
                <label> .cast/2 <width>, <symbol>;
            """
            node_dict[label] = Node(
                type_=type_name_table[opcode],      # look-up
                # actually only one
                inputs=make_inputs(statement["operands"][1:]),
                scope=scope,
                width=statement["operands"][0],     # Output width
                params={}
            )

        # [Node] Type casting statements to real
        elif opcode and opcode in [".cast/real", ".cast/real.s"]:
            """
            Type convertion
                <label> .cast/real <symbol>;
                <label> .cast/real.s <symbol>;
            """
            print("Unsupported opcode:", opcode)

        # [Node] User-defined functions FIXME: Not completely supported
        elif opcode and opcode == ".ufunc/vec4":
            """
            <label> .ufunc/vec4 <flabel>, <wid>,
                <isymbols> ( <psymbols> ) <ssymbol>;
            """
            #print("Unsupported .ufunc statement:", opcode)
            split_blank = statement["operands"][2].split(" ")
            node_dict[label] = Node(
                type_=type_name_table[opcode],          # look-up
                # net inoputs FIXME
                inputs=make_inputs([split_blank[0]]),
                scope=scope,
                width=statement["operands"][1],         # Output
                params={
                    "flabel": statement["operands"][0],
                    # Variables that represent the input ports for the function.
                    "represent_symbol": split_blank[1],
                    # The function scope name
                    "scope_symbol": split_blank[2],
                }
            )
        elif opcode and opcode.split("/")[0] == ".ufunc":
            print("Unsupported .ufunc statement:", opcode)

        # [Node] User-defined primitive instance. 1-bit functor.
        elif opcode and opcode == ".udp":
            node_dict[label] = Node(
                type_=type_name_table[opcode],             # look-up
                # FIXME: Check if correct.
                inputs=make_inputs(statement["operands"][1:]),
                scope=scope,
                width=1,                 # Output width.
                params={"def_type": statement["operands"][0]}
            )

        # [*Node] Islands
        elif opcode and opcode in [
            ".island", ".export", ".port", ".import",
            ".tran", ".tranif0", ".tranif1", ".tranvp",
            ".rtran", ".rtranif0", ".rtranif1"
        ]:
            print("Unsupported opcode: <Islands>", opcode)

        # [*Node] FIXME: What is this?
        elif opcode and opcode in [".sfunc", ".sfunc/e"]:
            print("Unsupported opcode: S-func", opcode)

        # [Node]
        elif opcode and opcode == ".repeat":
            """
            <label> .repeat <wid>, <rept count>, <symbol> ;
            """
            node_dict[label] = Node(
                type_ = type_name_table[".repeat"],
                inputs = make_inputs(statement["operands"][2:]),
                width = statement["operands"][0],
                scope = scope,
                params = {"rept_count":statement["operands"][1]}
            )
            print("Unfully supported opcode:", opcode)

        # [Node] Module path delay
        elif opcode and opcode == ".modpath":
            """
            .modpath <width> <input> , [ <src> (<delays> [? <condition>]) ] ;
            """
            input = statement["operands"][0].split(" ")
            node_dict[label] = Node(
                type_=type_name_table[".modpath"],
                #inputs=make_inputs(statement["operands"][1:]),
                inputs = [],
                width=input[0],
                params = {"delays": get_brac_str(statement["operands"][1])},
                scope=scope,
            )
            print("Unfully supported opcode:", opcode)

        # [Node] Events are  special nodes
        elif opcode and opcode == ".event":
            """
            <label> .event <type>, <symbols_list>;
            <label> .event "name";
            type: 
                edge
                posedge
                negedge
            or 
                named
            """
            type_ = statement["operands"][0]
            # FIXME: it may refer to net...

            node_dict[label] = Node(
                type_=type_name_table[".event " + type_],  # look-up
                inputs=make_inputs(statement["operands"][1:]),
                width=-1,  # FIXME： There should be a width.
                scope=scope,
            )
            # For later correction(from net-ref to node-ref)
            event_net_index[label] = statement["operands"][1:]

        # [Node] event/or
        elif opcode and opcode == ".event/or":
            """
            <label> .event/or <symbols_list>;
            """
            # FIXME: it may refer to net...
            node_dict[label] = Node(
                type_=type_name_table[".event " + type_],  # look-up
                inputs=make_inputs(statement["operands"]),
                width=-1,  # FIXME： There should be a width.
                scope=scope,
            )
            # For later correction(from net-ref to node-ref)
            event_net_index[label] = statement["operands"][1:]

        # [Node] Substitute
        elif opcode and opcode == ".substitute":
            """
            Will not appear in verilog.
            """
            print("Unsupported opcode:", opcode)

        # [Signal][Node] Vars are special nodes
        elif opcode and opcode == ".var":
            msb, lsb = statement["operands"][1].split(' ')
            width = abs(int(msb)-int(lsb))+1
            name = quote_remove(statement["operands"][0])
            signal_dict[label] = Signal(
                type_=Signal.T_VAR,
                scope=scope,
                msb=msb,
                lsb=lsb,
                name=name,
                val_labels=[label],             # Only one.
            )
            node_dict[label] = Node(
                type_=type_name_table[opcode],  # look-up
                inputs=[NodeInput("C4<"+"x"*width+">")],                      # Vars have no inputs
                scope=scope,
                width=width,
                params={}
            )
            scope_dict[scope].add_signal(name, label)
        elif opcode and opcode.split("/")[0] == ".var":
            print("Unsupported .var statement:", opcode)

        # [Signal] Nets are not nodes
        # /2u means Unsigned bool/bit net
        elif opcode and opcode in [".net", ".net8", ".net/2u"]:
            """
            General:
                <label> .net      "name"         , <msb>, <lsb>, <symbol>;
            From Array:
                Not defined in readme
                <label> .net   <arr_label> index , <msb>, <lsb>, <symbol>;
            """
            name_split = statement["operands"][0].split(' ')
            msb, lsb = statement["operands"][1].split(' ')
            name = quote_remove(statement["operands"][0])
            if len(name_split) == 1:
                # Is named net
                signal_dict[label] = Signal(
                    type_=Signal.T_NET,
                    scope=scope,
                    msb=msb,
                    lsb=lsb,
                    name=name,
                    # The value of this signal is the output of val_labels
                    val_labels=[statement["operands"][2]],  # Only one.
                )
                scope_dict[scope].add_signal(name, label)
            # FIXME: if there is a space in the quotes
            elif len(name_split) == 2:
                # Is array cell, append driver to array val_labels.
                arr = signal_dict.get(name_split[0])
                # The ".array" statement should be in front of the cells.
                assert arr
                assert len(arr.val_labels) == int(name_split[1])  # Val
                arr.val_labels.append(statement["operands"][2])
                arr.set_bit_range(msb,lsb)  # The net array bit-range is same as correspinding nets.

                # Also need to add to signal dict for event correction
                # Events may refer to nets, signal should be stored for later correction
                signal_dict[label] = Signal(
                    type_=Signal.T_NET_ARR_CELL,
                    scope=scope,
                    msb=msb,
                    lsb=lsb,
                    name="",
                    # The value of this signal is the output of val_labels
                    val_labels=[statement["operands"][2]],  # Only one.
                )
            else:
                raise Exception("Unknown net type")
        elif opcode and opcode.split("/")[0] in [".net", ".net8"]:
            print("Unsupported .net statement:", opcode)

        # [Signal] Array is not Node
        elif opcode and opcode == ".array":
            """
            Alias
                <label> .array "name", <src> ;
            Wire(Net) array
                <label> .array "name", <last> <first> ;
            Reg(Var) array
                Not defined in readme
                <label> .array "name", <last> <first>, <msb> <lsb> ;
                Store values...

                .array     
                .array/2s  
                .array/2u  
                .array/i   
                .array/obj 
                .array/real
                .array/s   
                .array/str 
            """
            arr_last, arr_first = statement["operands"][1].split(' ')
            name = quote_remove(statement["operands"][0])
            if len(statement["operands"]) == 3:
                # Is val array, should store all values
                msb, lsb = statement["operands"][2].split(' ')
                signal_dict[label] = Signal(
                    type_=Signal.T_VAR_ARR,
                    scope=scope,
                    msb=msb,
                    lsb=lsb,
                    arr_last=arr_last,
                    arr_first=arr_first,
                    name=name,
                )
            elif len(statement["operands"]) == 2:
                # Is net array, no values should be stored
                signal_dict[label] = Signal(
                    type_=Signal.T_NET_ARR,
                    scope=scope,
                    msb=-1,  # Temp
                    lsb=-1,  # Temp
                    arr_last=arr_last,
                    arr_first=arr_first,
                    name=name,
                )
            else:
                raise Exception("Unknown array type..")
            scope_dict[scope].add_signal(name, label)

        # [Signal] Parameters
        elif opcode and opcode in [".param/l", ".param/b", ".param/str", ".param/r"]:
            """
            Different from the readme doc.
            Actually: 
            <label> .param/l <name> <local_flag> <file_idx> <lineno>, [+]<value>;
            "+" means signed, could never be "-"
            """
            name, local_flag, file_idx, lineno, = statement["operands"][0].split(
                " ")
            signal_dict[label] = Signal(
                type_=Signal.T_PARAM,
                scope=scope,
                msb=-1,  # Temp
                lsb=-1,  # Temp
                name=name,
                val_labels=[statement["operands"][1]]  # FIXME: Eval...
            )

        # [Scope] Not node
        elif opcode and opcode == ".scope":
            # Module or others

            # shortest form .scope LABEL;
            if len(statement["operands"]) < 2:
                continue        # Scope switching is done during parsing.
            elif len(statement["operands"]) < 4:            # No parent?
                # print(statement["operands"][1])
                instance_name, definition_name, file_, file_line_number = statement["operands"][1].split(
                    ' ')
                # target_dict["is_top"] = True if name == type_name else False
                scope_dict[label] = Scope(
                    type_=statement["operands"][0],
                    inst_name=instance_name,
                    def_name=definition_name,
                    parent="",   # NO parent
                )
            else:
                instance_name, definition_name, file_, file_line_number = statement["operands"][1].split(
                    ' '
                )
                scope_dict[label] = Scope(
                    type_=statement["operands"][0],
                    inst_name=instance_name,
                    def_name=definition_name,
                    parent=statement["operands"][3],   # NO parent
                )
            # statement["is_dff"]:True if args.dff == tokens[3].string.split()[1] else None,  # determine if the scope is a dff.

        # [Scope-param] port_info is not node
        # We can directly attach ports to scope because the scope statement is
        #   in front of the .port_info statements
        elif opcode and opcode == ".port_info":
            number, direction, _, name = statement["operands"][0].split(' ')
            scope_dict[scope].ports[number] = {
                "name": quote_remove(name),
                "direction": direction,
            }

        # [Scope-param] timescale of a scope
        elif opcode and opcode == ".timescale":
            time_unit, time_precision = statement["operands"][0].split(" ")
            scope_dict[scope].set_timescale(time_unit, time_precision)

        # [UDP-Def] Combinatorial UDP definition
        elif opcode and opcode == ".udp/comb":
            udp_def_dict[label] = UdpDef(
            	name=quote_remove(statement["operands"][0]),
                input_amount=statement["operands"][1],
                table_rows=[
                    quote_remove(row) for row \
                        in statement["operands"][2:]
                ],
            )

        # [UDP-Def] Sequential UDP definition
        elif opcode and opcode == ".udp/sequ":
            init_val = 'x' if statement["operands"][2]=='2' else statement["operands"][2] 
            udp_def_dict[label] = UdpDef(
                name=quote_remove(statement["operands"][0]),
                input_amount=statement["operands"][1],
                init_val=init_val,  # "2" means x
                table_rows=[
                    quote_remove(row) for row \
                        in statement["operands"][3:]
                ],
                )

        # [Thread] vThread, the thread have no label
        elif opcode and opcode == ".thread":
            thread_start_label = statement["operands"][0]
            start_line = label_vinst_index.get(thread_start_label)
            assert start_line != None
            thread_dict[thread_start_label] = VThread(
                start=start_line,              # Index in the code segment
                end=current_vinst_index,       # Index in the code segment
            )

        # [vInst] Procedural Code
        elif opcode and opcode[0] == "%":
            # TODO: do something later
            assert(current_vinst_index == len(code_segment))
            current_vinst_index += 1
            code_segment.append(statement)          # FIXME: do something here

        elif scope in [":HEADER", ":FOOTER"]:
            # TODO: do something later
            pass

        # Label ony
        elif label and not opcode:
            pass    # FIXME: Do something
        else:
            if verbose_flag:
                print("Unknown type of statement:", label, opcode)

    for node_label in out_width_unset_list:
        node = node_dict[node_label]
        arr = signal_dict[ node.params["array_label"] ]
        msb = arr.msb
        lsb=arr.lsb
        node.width =  abs(int(msb)-int(lsb))+1

    # Change code-reference to index
    for v_inst in code_segment:
        opcode = v_inst["opcode"]
        basic_opcode = opcode.split("/")[0]
        if basic_opcode in [r"%fork", r"%callf", r"%fork", r"%jmp"]:
            v_inst["operands"][0] = label_vinst_index[v_inst["operands"][0]]

    # event-ref correction(from net to node)
    # print("event-net-index", event_net_index)
    for e_label, inputs in event_net_index.items():
        e_node = node_dict[e_label]

        for index, input_str in enumerate(inputs):
            # print("istr",input_str)
            e_sig = signal_dict.get(input_str)
            if e_sig:
                assert e_sig.type in [Signal.T_NET,
                                      Signal.T_NET_ARR_CELL, Signal.T_VAR]
                assert len(e_sig.val_labels) == 1
                e_node.change_node_input(index, e_sig.val_labels[0])
                print("found sig ref in event", e_label,
                      "change to", e_sig.val_labels[0])

    # Connect nodes (add node output)
    for node_label, node in node_dict.items():
        # In a node, the driver could be node, net, var
        for driver in node.inputs:
            # print(driver)
            if driver.is_const():
                continue
            # The driver of current node is driven by a logic node;
            if node_dict.get(driver.val_str):
                node_dict[driver.val_str].add_output(node_label)
            else:
                if verbose_flag:
                    print("Unknown type of driver:",
                          driver.val_str, driver.val_type, "in node", node_label)

    # Add scope relations
    for label, scope in scope_dict.items():  # the "scope" inside loop is a copy...
        parent = scope.parent
        if not parent:
            continue
        scope_dict[parent].add_child(label, scope.def_name)

        ancestor = parent
        while scope_dict.get(ancestor):
            scope_dict[label].ancestor_chain.append(
                ancestor
            )
            ancestor = scope_dict[ancestor].parent

    return node_dict,      \
        signal_dict,    \
        thread_dict,    \
        scope_dict,     \
        udp_def_dict,   \
        code_segment
