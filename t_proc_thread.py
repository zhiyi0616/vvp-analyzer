from json_tools import JsonExportable

class VThread(JsonExportable):
    _exports = ["start","end"]
    def __init__(self,start:int,end:int):
        assert type(start) == int
        assert type(end) == int
        self.start = start
        self.end = end


class VInstruction(JsonExportable):
    def __init__(self,opcode:str):
        self.opcode = opcode

