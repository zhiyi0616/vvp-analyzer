from json_tools import JsonExportable


class NodeInput(JsonExportable):
    _exports = ["val_type", "val_str"]

    def __init__(
        self,
        input_str: str,
        description: str = "",  # Will not be exported
    ):
        self.val_type = ''
        self.val_str = ''

        sign = ""

        self.description = description

        assert type(input_str) == str

        # If is signed
        if input_str[0] in "+-":
            sign = input_str[0]
            input_str = input_str[1:]

        # If is int
        if input_str[0] in "1234567890":
            self.val_type = "CONST_VEC4"
            # There should be no '.' inside
            self.val_str = bin(int(sign+input_str))[2:]

        # If is real
        elif input_str[0] == '.':
            raise Exception("Maybe real number")

        # If is Cx<xxx>
        elif "<" in input_str:    # Cx<xxx>
            assert input_str[-1] == ">" and input_str[0] == "C" and input_str[2] == "<"
            self.val_str = sign+input_str[3:-1].lower()
            if input_str[1] == "4":
                self.val_type = "CONST_VEC4"
            elif input_str[1] == "8":
                self.val_type = "CONST_VEC8"
                raise Exception("Unsupported const type", self.val_type)
            elif input_str[1] == "r":
                self.val_type = "CONST_VECR"
                raise Exception("Unsupported const type", self.val_type)
            else:
                raise Exception("Unsupported const type")

        # Label
        else:
            assert ">" not in input_str
            self.val_type = "NODE_LABEL"
            self.val_str = input_str

    def is_const(self):
        if self.val_type in ["CONST_VEC4", "CONST_VEC8", "CONST_VECR"]:
            return True
        return False


class Node(JsonExportable):
    _exports = ["type", "inputs", "scope", "width", "params", "outputs"]

    def __init__(
        self,
        type_: str,
        inputs: list,   # list of NodeInput
        scope: str,
        width: int,     # Output width
        params: dict = {},
    ):
        self.outputs = []

        assert type(inputs) == list
        width = int(width)
        self.type = type_
        self.inputs = inputs
        self.scope = scope
        self.width = width
        self.params = params

    def add_output(self, output):
        self.outputs.append(output)

    def change_node_input(self, index, input_str: str):
        self.inputs[index] = NodeInput(input_str)
