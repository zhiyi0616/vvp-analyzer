from vvp import Parser
import analyse
import json
import argparse
import copy
from json_tools import to_dict

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument(
    "-v", "--verbose", 
    help="Verbose output", action='store_true'
)
arg_parser.add_argument(
    "-o", "--output-file",
    help="Output JSON file", default='netlist.json'
)
arg_parser.add_argument(
    "--dff", 
    help="Define D flip-flop module name", type=str
)
arg_parser.add_argument("file", 
    help="Input vvp file name",
    default=None, nargs="?", type=argparse.FileType('r')
)
args = arg_parser.parse_args()

if args.file:
    # OUTS
    out_all = {}                  # The outer json container of all sections.
    statement_list = []         # To store all statements(parsed).
    # thread_dict = {}              # To store the thread finding result.

    # # TEMP
    # # thread_proc_list = []         # To temporarily store thread procs
    # label_index_dict = {}           # To help finding threads

    current_scope = None          # To indicate the scope in which the node lays
    have_err = False
    vvp_line_num = 0
    statement_index = 0
    
    prev_row = ""                 # For multi-line statements

    parser = Parser()
    # Read and parse
    for row in args.file.readlines():
        
        vvp_line_num += 1
        row = row.strip('\n')
        row = prev_row+row

        # Here a logic-row is read in
        result, error, finish_flag = parser.run(row,vvp_line_num)      # Passing the line number to parser
        # The currrent line(with prev) won't finish current statement
        if finish_flag == False:
            prev_row = row
            continue                        # Skip and read the next line
        prev_row = ""

        # Temporarily store the statement for thread finding.
        # thread_proc_list.append(result)

        if error:
            have_err = True
            print(error.as_string(), "line", vvp_line_num)
        elif result:
            result["statement_index"] = statement_index
            statement_list.append(result)
            # if result["label"]:
            #     label_index_dict[result["label"]] = statement_index
            statement_index += 1

        if args.verbose:
            if not vvp_line_num %10000:         # Print prompts if the file is large.
                print("%d rows parsed." % vvp_line_num)
    if have_err:
        print("Err occurred.")
        exit()
    
    
    # copy_statement_list=copy.deepcopy(statement_list)
    if args.verbose:
        print("Creating detailed statement information...")
    node_dict,      \
    signal_dict,    \
    thread_dict,    \
    scope_dict,     \
    udp_def_dict,   \
    code_segment =\
    analyse.fill_buckets(args.verbose, statement_list)
    node_amount = len(node_dict.keys())
    print("There are",node_amount,"nodes in this file.")
#     if args.verbose:
#         print("Attaching signal names to scopes...")
#     analyse.attach_signal_names(scope_dict, var_dict, net_dict)
# 
#     if args.verbose:
#         print("Adding node relationship...")
#     analyse.add_logic_relation(
#         args.verbose, node_dict, var_dict, net_dict
#     )
# 
#     if args.verbose:
#         print("Adding scope relationship...")
#     analyse.add_scope_relation(args.verbose, port_list, scope_dict)

    if args.verbose:
        print("Dumping JSON...")
    # Dump
    #out_all["statements"] = statement_list
    out_all["nodes"] = node_dict
    out_all["signals"] = signal_dict
    out_all["threads"] = thread_dict
    out_all["scopes"] = scope_dict
    out_all["udp_definitions"] = udp_def_dict
    out_all["code_segment"] = code_segment


    with open(args.output_file, "w+") as f:
        json.dump(out_all, f, indent=4, default = to_dict)

else:       # Interactive mode
    prev_row = ""
    finish_flag = True
    parser = Parser()
    print("You are now in interactive mode. Type '$' to quit.")
    while True:
        input_prompt = "vvp_parser > " if finish_flag else "       ... > "
        row = prev_row + input(input_prompt)
        if row and row[0] == "$":
            finish_flag = True
            exit()
        else:
            vvp_line_num = 0                    # The interactive mode does not have a line number
            result, error, finish_flag = parser.run(row,vvp_line_num)
        if not finish_flag:
            prev_row = row
            # print("prev_row:", prev_row)
            continue                        # Skip and read the next line
        prev_row = ""
        if error:
            print(error.as_string())
        else:
            print(result)
